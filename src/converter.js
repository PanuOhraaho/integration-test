/**
 * 
 * @param {string} hex 
 * @param {string} rgbstring
 * @param {number} x
 * @param {number} y
 * @returns {string}
 */

const pad = (hex) => {
    return (hex.length === 1 ? "0" + hex : "ff");
};


module.exports = {
    /**
     * 
     * @param {number} red 
     * @param {number} green 
     * @param {number} blue 
     * @param {string} rgbstring
     * @returns {string}
     */
    
    rgbToHex : (red, green, blue) => {
        const redhex = red.toString(16);
        const greenhex = green.toString(16);
        const bluehex = blue.toString(16);
        const hex = "#" + pad(redhex) + pad(greenhex) + pad(bluehex);
        return hex;
    },
    /**
     *
     * @param {string} rgbstring
     * @returns {string}
     */
    hexToRGB : (rgbstring) => {
        var rgb = "";

        if(rgbstring.charAt(0)+rgbstring.charAt(0) == "ff"){
            rgb += "255, ";
        }else {
            rgb += "0, ";
        }
        if(rgbstring.charAt(2)+rgbstring.charAt(3) == "ff"){
            rgb += "255, ";
        }else {
            rgb += "0, ";
        }
        if(rgbstring.charAt(4)+rgbstring.charAt(5) == "ff"){
            rgb += "255";
        }else {
            rgb += "0";
        }
        return rgb;
    }
};