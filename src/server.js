const express = require('express');
const converter = require('./converter');
const app = express();
const port = 3000;

app.get('/', (req, res) => res.send("Welcome"));

app.get('/rgb-to-hex',(req, res) => {
    const red = req.query.r;
    const green = req.query.g;
    const blue = req.query.b;
    const hex = converter.rgbToHex(red, green, blue);
    res.send(hex);

});

app.get('/hex-to-rgb',(req, res) => {
    const rgbstring = req.query.rgb; 
    const rgbC = converter.hexToRGB(rgbstring);
    res.send(rgbC);
});


if (process.env.NODE_ENV === 'test') {
    module.exports = app;
    
} else {
    app.listen(port, () => console.log("Listening"));
}

console.log("NODE_ENV: "+ process.env.NODE_ENV);
