// TDD - unit testing

const expect = require("chai").expect;
const converter = require("../src/converter");

describe("Color code converter", () => {
    describe("RGB to Hew convertions", () => {
        it("converts the basic colors", () => {
            const redhex = converter.rgbToHex(255, 0, 0); // #ff0000
            expect(redhex).to.equal("#ff0000"); 

            const bluehex = converter.rgbToHex(0, 0, 255); // #0000ff
            expect(bluehex).to.equal("#0000ff"); 
            
            const greenhex = converter.rgbToHex(0, 255, 0); // #0000ff
            expect(greenhex).to.equal("#00ff00"); 
            
        });
    });
    describe("Hex to RGB conversions", () => {
        it("conberts the basic colors", () => {
            const redrgb = converter.hexToRGB("ff0000");
            expect(redrgb).to.equal("255, 0, 0");

            const greenrgb = converter.hexToRGB("00ff00");
            expect(greenrgb).to.equal("0, 255, 0");

            const bluergb = converter.hexToRGB("0000ff");
            expect(bluergb).to.equal("0, 0, 255");
        })
    })
});